/*
 * Copyright Yannik Henke
 * www.mineyannik.de
 * All Rights reserved
 */

package de.mineyannik.easyprefix.listeners;

import de.mineyannik.easyprefix.Main;
import net.ess3.api.events.NickChangeEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author yannikhenke
 */
public class EssentialsListener implements Listener {
    
    private final Main plugin;
    
    public EssentialsListener(Main plugin)
    {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onNick(NickChangeEvent e)
    {
        Main.essentialsnickname.put(e.getAffected().getBase().getUniqueId(), e.getValue());
    }
}
