package de.mineyannik.easyprefix.listeners;

import de.mineyannik.easyprefix.Main;
import net.ess3.api.IUser;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener
  implements Listener
{
  private final Main plugin;

  public ChatListener(Main plugin)
  {
    this.plugin = plugin;
  }

  @EventHandler
  public void onPlayerChat(AsyncPlayerChatEvent e)
  {
    String message = e.getMessage();
    Player p = e.getPlayer();
    String prefix = plugin.prefix.get(p.getUniqueId());
    
    
    if (prefix == null)
    {
        prefix = this.plugin.defaultprefix;
    }
    if (Main.essentialsnickname.containsKey(p.getUniqueId()) && Main.useEssentials)
    {
        e.setFormat(prefix + Main.essentialsnickname.get(p.getUniqueId()) + ": " + ChatColor.RESET + message);
        
        return;
    }
    e.setFormat(prefix + p.getName() + ": " + ChatColor.RESET + message);
  }
}