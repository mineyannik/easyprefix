package de.mineyannik.easyprefix;

import de.mineyannik.easyprefix.commands.MainCommand;
import de.mineyannik.easyprefix.listeners.ChatListener;
import de.mineyannik.easyprefix.listeners.EssentialsListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.MetricsLite;

public class Main extends JavaPlugin
{
  public String defaultprefix;
  public HashMap<UUID, String> prefix = new HashMap<>();
  public static HashMap<UUID, String> essentialsnickname = new HashMap<>();
  public static boolean useEssentials = false;
 
  @Override
  public void onEnable()
  {
    System.out.println("[EasyPrefix] Copyright by mineyannik");
    System.out.println("[EasyPrefix] All rights Reserved");

    //Plugin Metrics
    try {
        MetricsLite metrics = new MetricsLite(this);
        metrics.start();
    } catch (IOException e) {
        System.out.println("Failed to send stats to Plugin Metrics: " + e.getMessage());
    }
    
    Config();

    getServer().getPluginManager().registerEvents(new ChatListener(this), this);
    if (Bukkit.getPluginManager().isPluginEnabled("Essentials"))
    {
        this.getServer().getPluginManager().registerEvents(new EssentialsListener(this), this);
        useEssentials = true;
    } 
    //Kommando registrieren
    this.getCommand("easyprefix").setExecutor(new MainCommand(this));
  }
  
  @Override
  public void onDisable()
  {
      if (!essentialsnickname.isEmpty())
      {
          for (UUID u : essentialsnickname.keySet())
          {
              this.getConfig().addDefault("Easyprefix.nicks." + u.toString(), essentialsnickname.get(u));
          }
          
          this.saveConfig();
      }
  }

  
  public void Config()
  {
      this.reloadConfig();
      this.getConfig().addDefault("Easyprefix.defaultprefix", "[§1User§f] ");
      getConfig().addDefault("Easyprefix.default.prefix", "[§4Default§f] ");

      List liste = new ArrayList();
      liste.add("01e00e08-0468-4b33-acf2-e99a19c0ea6a");

      getConfig().addDefault("Easyprefix.default.members", liste);

      getConfig().options().copyDefaults(true);
      saveConfig();
      
      if (this.getConfig().contains("Easyprefix.nicks"))
      {
          for (String s : this.getConfig().getConfigurationSection("Easyprefix.nicks").getValues(false).keySet())
            { 
                essentialsnickname.put(UUID.fromString(s), this.getConfig().getString("Easyprefix.nicks." + s));
            }
      }
      
      //UUID Converter
      if (this.getConfig().getInt("version") < 2)
      {
          System.out.println("UUID Conversation started.");
          for (String s : this.getConfig().getConfigurationSection("Easyprefix").getValues(false).keySet())
          {
              try {
                  UUIDFetcher uf = new UUIDFetcher(this.getConfig().getStringList("Easyprefix." + s + ".members"));
                  List<String> us = new ArrayList<>();
                  for (UUID u : uf.call().values())
                  {
                      System.out.println("UUID of Player "  + Bukkit.getOfflinePlayer(u).getName() + " is " + u.toString());
                      us.add(u.toString());
                      
                  }
                  this.getConfig().set("Easyprefix." + s + ".members", us);
                  
              } catch (Exception ex) {
                  System.err.println("ERROR: " + ex.getMessage());
              }
          }
          System.out.println("UUID Conversation ended.");
          this.getConfig().set("version", 2);
          this.saveConfig();
      }
      
      for (String s : this.getConfig().getConfigurationSection("Easyprefix").getValues(false).keySet())
      {
          //Prefix auslesen
          String _prefix = this.getConfig().getString("Easyprefix." + s + ".prefix");
          
          //Spieler auslesen
          for (String p : this.getConfig().getStringList("Easyprefix." + s + ".members"))
          {
              prefix.put(UUID.fromString(p), _prefix);
          }
          
        
          //Default prefix setzen
          this.defaultprefix = this.getConfig().getString("Easyprefix.defaultprefix");
      }
      
  }
/*
  Changelog:
 
  2.2:
  - Updatet to Java 1.7
  - Added Essentials /nick funktion.
  - Added /easyprefix add Kommando
  - Changed to UUID System
  
  2.1.2
  - Updated to new Bukkit version.
  
 2.1.1:
  - Bugfix
  
 2.0:
  Updated to 1.7.2
  Code optimations
  Added possibilty to add your own groups
  Code optimations
  Added /easyprefix reload command (easyprefix.reload)
  
  You need to recreate your config file.
  
 2.1:
  - Bugfix
  - Added Plugin Metrics
  */
  /*
  TODO:
  - API
  */
}