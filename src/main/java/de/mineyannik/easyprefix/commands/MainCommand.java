/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.mineyannik.easyprefix.commands;

import de.mineyannik.easyprefix.Main;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author yannikhenke
 */



public class MainCommand implements CommandExecutor {

    private final Main plugin;

    public MainCommand (Main plugin)
    {
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String string, String[] args) {
        
        if (args.length == 0)
        {
            cs.sendMessage(ChatColor.GOLD + "EasyPrefix Commands:");
            cs.sendMessage(ChatColor.GREEN + "- /EasyPrefix reload | Reloads the Plugin");
            cs.sendMessage(ChatColor.GREEN + "- /EasyPrefix add    | Adds a user to a group.");
            return true;
        }
        
        if (args[0].equalsIgnoreCase("reload"))
        {
            if (!cs.hasPermission("easyprefix.reload"))
            {
                cs.sendMessage(ChatColor.RED + "You don't have enough Permissions to do that.");
                return true;
            }
        
            //Reload Plugin
            Bukkit.getServer().getPluginManager().disablePlugin(plugin);
            Bukkit.getServer().getPluginManager().enablePlugin(plugin);
        
            //Spieler informieren
            cs.sendMessage(ChatColor.GREEN + "Plugin reloaded.");
        
            return true;
        }
        
        if (args[0].equalsIgnoreCase("add"))
        {
            if (args.length < 2)
            {
                cs.sendMessage("/easyprefix add <username> <group>");
                return true;
            }
            if (!cs.hasPermission("easyprefix.add"))
            {
                cs.sendMessage(ChatColor.RED + "You don't have enough Permissions to do that.");
                return true;
            }
            plugin.reloadConfig();
            String username = args[1];
            String group = args[2];
            
            OfflinePlayer op = Bukkit.getOfflinePlayer(username);
            
            if (op == null)
            {
                cs.sendMessage("Player not found.");
                return true;
            }
            
            if (!plugin.getConfig().contains("Easyprefix." + group + ".members"))
            {
                cs.sendMessage("Group not found.");
                return true;
            }
            
            List<String> list = plugin.getConfig().getStringList("Easyprefix." + group + ".members");
            list.add(op.getUniqueId().toString());
            plugin.getConfig().set("Easyprefix." + group + ".members", list);
            plugin.saveConfig();
            cs.sendMessage("Added");
        }
        return false;
        
    }
    
    
    
}
